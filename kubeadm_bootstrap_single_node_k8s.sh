# escalate privileges
sudo su

# install tools
apt-get update
apt-get upgrade -y
apt-get install -y apt-transport-https ca-certificates curl gpg docker.io containerd
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq
chmod +x /usr/bin/yq

# host preperation
## disable swap
swapon --show
swapoff -a
sed -i '/swap/d' /etc/fstab
swapon --show
## kernal module parameters
cat /etc/sysctl.d/k8s.conf <<EOF
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
sysctl --system
## kernal modules
echo overlay       > /etc/modules-load.d/k8s.conf
echo br_netfilter >> /etc/modules-load.d/k8s.conf
modprobe br_netfilter
modprobe overlay
## container runtime
mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml
sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
systemctl restart containerd

# cluster creation
## control plane
kubeadm init --pod-network-cidr=192.168.0.0/16
## non-root authentication
exit
mkdir -p ~/.kube
sudo cp /etc/kubernetes/admin.conf ~/.kube/config
sudo chown $(id -u):$(id -g) ~/.kube/config
## untaint control plane node(s)
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
## cni setup
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.27.2/manifests/tigera-operator.yaml
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.27.2/manifests/custom-resources.yaml  # adjust pod cidr in this manifest if different from default

# various troubleshooting
kubectl get componentstatuses
systemctl status containerd
systemctl restart kubelet
journalctl -fexu kubelet
systemctl daemon-reload

# cleanup
## cluster termination
kubeadm reset
rm -rf /etc/cni/net.d/

