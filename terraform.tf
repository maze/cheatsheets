# note: EC2 instance `user_data` runs as `root` user.

## provider with default tags that will propagate to most other resources in project
provider "aws" {
  default_tags {
    tags = {
      Owner = local.local_username
    }
  }
}

# sets a terraform local variable from a system environment variable
locals {
  local_username = env.USER
}

# lookup available AZs in current region; will expose an attribute `aws_availability_zones.available.names`
data "aws_availability_zones" "available" {
  state = "available"
}

# lookup AMI IDs by OS image basename
data "aws_ami_ids" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]
  filter {
    name   = "name"
    values = ["ubuntu/images/ubuntu-*-*-amd64-server-*"]
  }
}

# lookup local workspace's public IP (for usage in security group source, etc.); expose in CIDR format `"${chomp(data.http.allowedip.response_body)}/32"`
data "http" "allowedip" {
  url = "http://ipv4.icanhazip.com/"
}

# will yield ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]; add more subnets with additional `, 8`
locals {
  cidrs = cidrsubnets("10.0.0.0/16", 8, 8, 8, 8)
}
