[click here](https://pandoc.org/MANUAL){target=_blank} to open a link in a new tab (works with Pandoc).

<details>
  <summary>Click for more details...</summary>

Here are the details!
This technically isn't <em>Markdown</em>, but usually one can embed some <em>HTML</em> into it - which yields this!
</details>

