# bash
gpg --edit-key KEY_NAME  # open key KEY_NAME in gpg for editing.
gpg --armor --export             --output NAME.asc.pub KEY_NAME  # export public key component from KEY_NAME to file starting with NAME.
gpg --armor --export-secret-keys --output NAME.asc.key KEY_NAME  # export private key component from KEY_NAME to file starting with NAME.
gpg --armor --gen-revoke         --output NAME.asc.rev KEY_NAME  # export revocation certificate for KEY_NAME to file starting with NAME.
gpg --list-keys --keyid-format long  # list keys with long id format, good for getting long key id for sending key to keyserver or configuring gitconfig to sign commits.
gpg --send-keys KEY_ID  # pushes key KEY_NAME to a keyserver.
gpg --export KEY_NAME | curl -T - https://keys.openpgp.org  # upload public key to keyserver; returns link for e-mail verification.

# gpg
showpref  # can show key notations.
notation a@b=c # for adding notations.
notation -a@b=c # for removing notations.
fpr  # show key fingerprint
primary  # flag the selected user ID as primary.
change-usage  # change key usage (certify/sign/encrypt).
