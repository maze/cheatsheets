# Varmilo VA87M keyboard
| Shortcut          | Function                                                                                               |
|-------------------|--------------------------------------------------------------------------------------------------------|
| `Fn + Up        ` | Increase backlight                                                                                     |
| `Fn + Down      ` | Decrease backlight                                                                                     |
| `Fn + Right     ` | Cycle backlight modes (static/breathing)                                                               |
| `Fn + Right Win ` | Hold for 3 seconds to switch Fn key with Right Win key (to switch back, press Left Win first, then Fn) |
| `Fn + Esc       ` | Hold for 3 seconds to reset settings (if switched with Win key, hold Left Win  + ESC)                  |
| `Fn + Left Ctrl ` | Switch Caps Lock and left Ctrl (use Caps Lock key to switch back!)                                     |
| `Fn + W         ` | Hold for 3 seconds to switch to Windows mode                                                           |
| `Fn + A         ` | Hold for 3 seconds to switch to Apple mode                                                             |

# Razer Huntsman Mini keyboard LED modes
| Shortcut        | Function         |
|-----------------|------------------|
| `Fn + Ctrl + 1` | Off              |
| `Fn + Ctrl + 2` | Static           |
| `Fn + Ctrl + 3` | Breathing        |
| `Fn + Ctrl + 4` | Spectrum cycling |
| `Fn + Ctrl + 5` | Wave             |
| `Fn + Ctrl + 6` | Reactive         |
| `Fn + Ctrl + 7` | Starlight        |

# GNOME Shell
- `Alt + F2`, `r`, then `Enter` - to restart the GNOME Shell.
