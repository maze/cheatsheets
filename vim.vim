" general
:%s/\v[0-9]/x/  " search and replace with regular expression (`\v`).
:set paste  " allows correct pasting into vim when auto-indent is on.
diw  " delete the entire word under the cursor (regardless of position).
gd  " go to definition of object under cursor.

" marks
mX  " creates a mark X at current context (where X is [a-zA-Z]).
'X  " go to mark X.

" folding
:set foldmethod=indent  " select folding method (manual/indent/syntax...).
za  " toggles openong/closing of fold under cursor.
zd  " delete fold under cursor.
vzf  " creates a fold for visually selected lines (requires foldmeth=manual).
zk  " jump to previous fold.
zj  " jump to next fold.

" jumplist
C-i  " go forwards to newer cursor positions in jumplist.
C-o  " go backwards to older cursor positions in jumplist.
