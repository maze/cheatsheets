#!/usr/bin/env python

# create a virtual python environment (in current directory)
python -m venv .
# use the virtual environment's binaries to install package PACKAGE and run script SCRIPT.
./bin/pip install PACKAGE_NAME
./bin/python SCRIPT.py

# logging
import logging
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(funcName)s: %(message)s",
    # filename="app.log",
)
logging.info("hello log!")

# ip address validation
import ipaddress
try:
    ipaddress.ip_address("111.222.333.444")
except:
    pass

# print python dictionary in yaml
import yaml  # requires package `pyyaml`
dictionary = {"fook": "bar", "l": [1, 2, 3]}
print(yaml.dump(dictionary, sort_keys=False))  # `sort_keys` is optional

# regular expressions
import re
if re.findall(r"(re)gex?", "hello regex"):
    print("found pattern!")

# print in color
from termcolor import colored
print("hello" + colored("world", "green"))

# oneline conditional
age_group = "minor" if age < 18 else "adult"

# expand a tuple into function args
t = ("hello", "world")
def f(a, b):
    print(a, b)
f(*t)
