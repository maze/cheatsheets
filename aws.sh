aws sts decode-authorization-message --encoded-message ${YOUR_ENCODED_ERROR} | jq '.DecodedMessage' | sed 's/\\"/"/g' | sed -E 's/"(.+)"/\1/' | jq  # when receiving an encoded authentication error message, you can decode it using the AWS CLI like so:

aws ecs execute-command --cluster MY_CLUSTER --task MY_TASK_ID --container CONTAINER_NAME --command "/bin/bash" --interactive  # like `docker exec CONTAINER_NAME -it bash` for aws ecs tasks.

# grow volume of EC2 instance by instance ID
aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --output text --query 'Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId'  # get volume ID.
aws ec2 modify-volume --volume-id ${VOLUME_ID} --size ${NEW_SIZE_IN_GB}  # grow EBS volume.
sudo growpart /dev/${DISK_NAME} ${PARTITION_NUMBER}  # check with lsblk; example values: `xvda 1`.
sudo resize2fs /dev/${PARTITION_NAME}  # example value: xvda1.

# add tag to all instances by name pattern
NAME_PATTERN="foo-*"
instance_ids=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=${NAME_PATTERN}" --query "Reservations[].Instances[].InstanceId" --output text)
for instance_id in ${instance_ids}; do
    aws ec2 create-tags --resources ${instance_id} --tags Key=<tag-key>,Value=<tag-value>
done

# things to cleanup when struggling to delete a VPC due to "zombie" network interfaces (that refuse to delete):
# - Load Balancers (classic)
# - NAT Gateways
# - RDS proxies
