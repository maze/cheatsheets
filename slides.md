---
# yaml metadata
title: Pandoc
subtitle: How to make a slideshow using Markdown
author: Michael Zeevi
date: \today
lang: en-US
aspectratio: 169
colorlinks: true
colortheme: default  # other nice ones are: beaver, crane, default, dove
# background-image: ~/Pictures/space.png  # slides' background
---

# Advantages of Markdown
- **Plaintext** - lightweight, portable and compatible (opposed to _Rich Text_ formats).
- Simple, yet powerful - allows the user to remain focused on content writing.
- Ubiquitous - commonly used with many tools (e.g. Jira, Notion, Git repositories' `README.md` files, etc.).

![[Markdown](https://en.wikipedia.org/wiki/Markdown) logo](res/markdown.png){height=100px}


# Pandoc
- A free and open-source document converter.
- List of [supported formats](https://en.wikipedia.org/wiki/Pandoc#Supported_file_formats).

![[Pandoc](https://pandoc.org/) logo (unofficial)](res/pandoc.png){height=100px}


# Instructions
1. Install prerequisites:
   - Pandoc
   - A PDF engine (such as TexLive)
     ```bash
     apt install -y pandoc texlive texlive-xetex texlive-pictures
     ```
2. Write presentation in Markdown (this).
3. Convert using Pandoc:
   ```bash
   pandoc slides.md --to beamer --output slides.pdf
   ```
   - **Input:** Markdown.
   - **Output:** [_Beamer_](https://ctan.org/pkg/beamer) presentations (`*.pdf`).

## Note:
_You can use [variables for Beamer slides](https://pandoc.org/MANUAL.html#variables-for-beamer-slides) in the Markdown file's front matter._


# Bonus topic: Makefiles
- A common practice when working with Pandoc, is to write a simple [_Makefile_](https://en.wikipedia.org/wiki/Make_(software)#Makefile)...
- A Makefile stores the converstion command(s) along with any required arguments.
- Here's an example Makefile:
  ```make
  .PHONY: slides
  slides:
    pandoc slides.md -t beamer -o slides.pdf
  ```
- Quicker to just run `make`.


# Thank you for your time

\begin{center}
\Huge
Questions?
\normalsize
\end{center}
