# k8s
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml  # setup monitoring with metrics-server, then can use `kubectl top ...`.
kubectl get pods -o custom-columns='NAME:metadata.name,IMAGE:spec.containers[*].image'  # list images used in all containers in all pods.
kubectl get svcs -o custom-columns='NAME:metadata.name,PORTS:.spec.ports[*].port' svc  # get all external ports of all services.
kubectl patch configmap www -p "$(cat patch.yaml)"  # applied changes in patch.yaml to configmap 'www'.
kubectl wait --for=condition=available deployment web  # wait for deployment 'web' to be available.
kubectl wait --for=condition=ready pod -l app=foo  # wait for pods with label 'app=foo' to be ready.
kubectl wait --for=condition=complete job/myjob  # wait for job 'myjob' to be complete.
kubectl expose deployment web-deployment --name web-svc --port=80 --target-port=5000  # imperativley create service web-svc for deployment web-deployment.
kubectl port-forward svc/SERVICE_NAME 1234:5601  # expose SERVICE_NAME service over localhost:1234.
kubectl exec -it mongodb-123-xyz -- mongodump -u mongo -p password --archive --gzip | aws s3 cp - s3://my-mongo-dumps/$(date +'%Y-%m-%d').tar.gz --storage-class STANDARD_IA --sse  # backup mongo tables and save to s3 bucket.
kubectl create ingress my-ing --rule "mydomain.com/my-path/*=my-svc:5000"  # imperatively create ingress 'my-ing' with a rule mapping 'mydomain.com' with the prefix (`*`) path '/my-path/' to the service 'my-svc' (port 5000).
kubectl create ingress my-ing --rule "mydomain.com/my-path/*=my-svc:5000,tls=tls-mydomain" --annotation cert-manager.io/cluster-issuer=letsencrypt-staging  # same us above, but with tls (if cert-manager and a clusterissuer are not present, then will use a fake certificate, provided by the ingress controller).
kubectl create ingress restaurant --dry-run=client -o yaml --annotation nginx.ingress.kubernetes.io/use-regex="true" --annotation nginx.ingress.kubernetes.io/rewrite-target=/\$2 --rule "restaurant.com/food(/|\$)(.*)*=food:5000" --rule "restaurant.com/drink(/|\$)(.*)*=drink:5000"  # a more advanced example of an imperative ingress creation command, including escape characters and target rewrite annotations.
NAMESPACE=foo ; kubectl get namespace $NAMESPACE -o json | jq 'del(.spec.finalizers[0])' | kubectl replace --raw "/api/v1/namespaces/$NAMESPACE/finalize" -f -  # if a "zombie" namespace 'foo' is stuck in "terminating" state, this will help killing it.
kubectl apply -f - <<-EOF  # apply a resource using a here-doc
  apiVersion: v1
  kind: Namespace
  metadata:
    name: test
EOF

# helm
helm install RELEASE_NAME CHART_NAME --set selector.matchLabels.'kubernetes\.io/metadata\.name'=default  # set a helm value that contains periods (use quotes and escape the periods).
helm get values RELEASE_NAME  # prints the values of a helm release.

# https://github.com/Opsfleet/depends-on utilizes kubernetes resources to create a pod startup order (dependencies) mechanism.

# k3s
curl -sfL https://get.k3s.io | sh -  # installs local lightweight kubernetes cluster that will restart if host restarts.
k3s server --write-kubeconfig-mode 644  # spin up a local k3s cluster, allowing non-root authentication via `export KUBECONFIG=/etc/rancher/k3s/k3s.yaml`.

# argo cd
argocd app create my-app --dest-name in-cluster --sync-policy automated --auto-prune --self-heal --sync-option CreateNamespace=true --repo https://some.scm/some-repo --path ./my-apps --dest-namespace staging  # create an application 'my-app' from the source `some.scm/some-repo/my-apps`, in the namespace 'staging' local cluster (`in-cluster`). application is configured to auto-sync changes from git (including deletions/prunes), self-heal (any manual drifts made in runtime), and create its 'staging' namespace (if doesn't exist).
kubectl get applications -A -o=jsonpath='{range .items[?(@.status.health.status=="Missing")]}{.metadata.name}{"\n"}' | xargs -I {} kubectl -n argocd patch application {}  --type=json -p='[{"op": "remove", "path": "/metadata/finalizers"}]'  # sometimes applications refuse to be deleted due to buggy finalizers, this will force delete such zombies (assuming 'argocd' namespace).

# nginx ingress controller rewrite annotation
metadata:
  name: restaurant
  annotations:  #  https://github.com/kubernetes/ingress-nginx/blob/main/docs/examples/rewrite/README.md
    nginx.ingress.kubernetes.io/use-regex: "true"
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
  - host: restaurant.com
    http:
      paths:
      - path: /food($|/)(.*)
        pathType: Prefix
        backend:
          service:
            name: kitchen
            port:
              number: 80
      - path: /drink($|/)(.*)
        pathType: Prefix
        backend:
          service:
            name: bar
            port:
              number: 80

