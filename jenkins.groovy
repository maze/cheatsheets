// get initial containerized jenkins admin password 
docker-compose exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword

// script console
Jenkins.instance.getItemByFullName("JOBNAME").updateNextBuildNumber(666)  // input in 'manage jenkins'->'script console'. changes the next build's number to 666 for job JOBNAME.
println "Job type: ${Jenkins.getInstance().getItem("MYJOB").getClass()}"  // input in 'manage jenkins'->'script console'. prints the job type of job MYJOB.
// adding users
import jenkins.model.*
import hudson.security.*
def instance = Jenkins.getInstance()
def hudsonRealm = new HudsonPrivateSecurityRealm(false)
def users = [
  alice: "900fec6d3c1b3806de92f935e5573b8b",
  bob: "747b458a3f4e56ddd471fda3466d78e1",
  charlie: "1df7e41d61369b04c8bf8c155a999211",
  dan: "0aa8e7bc3b4509c0d8637ce6092d0e87"
]
users.each { username, password ->
  hudsonRealm.createAccount(username, password)
}
instance.setSecurityRealm(hudsonRealm)
instance.save()
// install plugins from list
def pluginsToInstall = [
  'active-directory',
  'ansicolor',
  'artifactory',
]
for (plugin in pluginsToInstall) {
  println "Installing '$plugin'..."
  e = Hudson.instance.updateCenter.getPlugin(plugin).deploy().get().getError()
  if (e != null) println e.message
}

// scripted pipeline one-liners
script {
  sleep(new Random().nextInt(20))  // sleep for a random amount of seconds (up to 20).
  x = sh(returnStdout: true, script: "echo value").trim()  // capture shell output into var x.
  (x, y, z) = v.tokenize(".")  // split string at dots to variables.
  currentBuild.description = "..."  // set a description per build.
  currentBuild.result = "UNSTABLE"  // set a build to pass, but be marked unstable (yellow).
}

// declerative pipeline one-liners
sh("mvn help:effective-pom -Doutput=/tmp/effective-pom.xml")  // save effective-pom.xml to output file.
stash(includes: "target/", name: "build_output")  // stashes a workspace artifact (such as "target/") between stages/workers in a jenkins pipeline.
unstash("build_output")  // retrieves above stashed artifact to current workspace.

// state in build description if it was a replay - sometimes useful when debugging
try {
  currentBuild.description += " (${currentBuild.getBuildCauses('org.jenkinsci.plugins.workflow.cps.replay.ReplayCause')[0].shortDescription})"
} catch (e) {
  true
}

// fancy environment variable values
environment {
  // with bash
  GIT_BRANCH_CLEAN = "${sh(script: "echo '${env.GIT_BRANCH}' | sed 's@/@-@g'", returnStdout: true).trim()}"
  // with groovy
  DOCKER_TAG  = "${env.GIT_BRANCH == "main" ? "" : "snapshot-${env.GIT_BRANCH_CLEAN}-b${env.BUILD_NUMBER}"}"
}

// check if a git tag exists for current ref, otherwise use a default value (from env), for the docker tag variable
steps {
  sh("git fetch --tags $env.GIT_URL HEAD")
  script {
    DOCKER_IMAGE_TAG = sh(script: "git describe --tags --exact-match || echo '$env.DOCKER_DEFAULT_IMAGE_TAG'", returnStdout: true).trim()
  }
  sh("echo Will use tag: '$DOCKER_IMAGE_TAG'")
}

// AWS access key credential step wrapper
withCredentials([aws(credentialsId: "jenkins-aws", accessKeyVariable: "AWS_ACCESS_KEY_ID", secretKeyVariable: "AWS_SECRET_ACCESS_KEY", regionName: "eu-west-1")]) {
  sh("aws sts get-caller-identity")
}

// ssh credential step wrapper
withCredentials(bindings: [sshUserPrivateKey(credentialsId: "jenkins-ssh", keyFileVariable: "id")]) {
  sh("ssh -i $id user@rhost hostname")
}

// using the configuration files in jenkins pipelines
configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
  sh("mvn -s $MAVEN_SETTINGS clean package")
}

// retrieve environment variable from downstream job
script {
  buildResult = build(job: "downstream-job", wait: true)  // if job is unsuccessful, this will return `null`!
  X = buildResult.buildVariables.X
}

// jenkins evaluates environment variables as strings, to overcome this one must cast them (such as in the following conditional example)
when {
  expression {
    SOME_AMOUNT.toInteger() == 0 || FORCE.toBoolean()
  }
}

// dynamically define parallel stages
stage("Determine parallel stages dynamically") {
  steps {
    script {
      env.OPERATING_SYSTEMS = "debian centos arch"
    }
  }
}
stage("Run dynamic parallel stages") {
  steps {
    script {
      dynamicStages = [:]
      OPERATING_SYSTEMS.tokenize(" ").each { operatingSystem ->
        dynamicStages[operatingSystem] = {
          stage("Build for $operatingSystem") {
            echo "Mock building for '$operatingSystem'..."
          }
        }
      }
      parallel dynamicStages
    }
  }
}
// dynamically define a parallel pseudo-matrix
stage("Determine parallel stages dynamically") {
  steps {
    script {
      env.OPERATING_SYSTEMS = "debian centos arch"
      env.VERSIONS = "2.0 1.0"
    }
  }
}
stage("Run dynamic parallel stages") {
  steps {
    script {
      dynamicStages = VERSIONS.tokenize(" ").collectEntries { version ->
        OPERATING_SYSTEMS.tokenize(" ").collectEntries { operatingSystem ->
          ["${operatingSystem}-${version}": {
            stage("Build for $version on $operatingSystem") {
              echo("Mock building $version on $operatingSystem...")
            }
          }]
        }
      }
      parallel dynamicStages
    }
  }
}
