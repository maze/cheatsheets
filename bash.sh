#!/usr/bin/env bash

# system
sudo -e /etc/hosts  # edit /etc/hosts as root
sudo dd bs=4M if=~/Downloads/IMAGE.iso of=/dev/sdXX status=progress conv=fsync  # create a bootable usb from an imamge (first identify ouput file device using `lsblk`, then unmount with `sudo umount /dev/sdXX`)
sudo mkfs.vfat /dev/sdXX -n "USB"  # format a usb flash stick (`/dev/sdXX`) for normal storage, labeling the volume "USB".
uname -a  # show system information.
lsb_release -a  # show linux software base release details.
gsettings set org.gnome.shell.app-switcher current-workspace-only true  # makes application switcher will cycle only through applications in current workspace.
last  # Show a listing of last logged in users.
ls $(echo $PATH | tr ':' ' ') | grep -v '/' | grep . | sort  # list all commands by name
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u)  # show all manually installed packages.
history -d N  # deletes the Nth entry from history (for example a plain text password).
gnome-session-quit  # lock session.
echo $XDG_SESSION_TYPE  # check diplay server type
sudo sed -i -E 's/.+(set bell-style none)/\1/' /etc/inputrc  # disable annoying bell in bash.
xhost +SI:localuser:root  # allow root to run gui based apps in wayland.
# system tray icons (steam, keypass, etc.) can be displayed using the extension from: https://extensions.gnome.org/extension/1503/tray-icons/
# to use the X display server instead of wayland, edit `/etc/gdm3/custome.conf` (or `daemon.conf`) and uncomment the line `#WaylandEnable=false` then reboot the computer. this can help with issues with screen share/recording.
# https://command-not-found.com/ is a handy site to find best practice method to install various commands/applications with each package manager.
ps -e | grep tty  # helps checking which display server is running (look for x or wayland).
# in htop, pressing F5 switches to *tree* display for processes.
du -ha . | sort -h | tail -n 10  # list the 10 largest (storage) items under directory `.`.
espeak -k 20 'The CLI is my playground!'  # outputs speach with adjusted pitch for capital letters.
ps -ef  # see all running processes and their parent process ids (PPID).

# files
rename -v 's/OLD/NEW/' **  # substitute all OLD to NEW in all (**) files and folders here. Use -n for dry-run.
shred -uz -v FILE  # unrecoverably remove FILE, verbosely.
tail -n +2  # outputs from the 2nd line and below.
qpdf --decrypt IN.pdf --password=1234 OUT.pdf  # decrypt `IN.pdf` with password '1234' and save as an unencrypted `OUT.pdf`.
ffmpeg -i BIG_VIDEO.mp4 -ss 05:12 -to 35:59 -c:v libx264 -crf 18 -vf scale=640:-1 SMALL_VIDEO.mp4  # cuts BIG_VIDEO.mp4 to between 05:12 to 35:59 and resizes resolution to 640p, saving output to SMALL_VIDEO.mp4.
ffprobe -show_streams VIDEO.mp4 | grep 'H.264'  # check codec used in VIDEO.mp4.
ffmpeg -i VIDEO.mp4 -c:v libx264 VIDEO.264.mp4  # transcode video stream of `VIDEO.mp4` with codec `libx264` (to allow browser playback) and save output to `VIDEO.264.mp4`.
ffmpeg -f concat -i videos.lst -c copy OUTPUT.mp4  # concatenate mp4 videos listed in `videos.lst` (one per line, prefixed with "file ") and save to `OUTPUT.mp4`.
yq e -i '.image.tag = "latest"' values.yaml  # updates the value of `.image.tag` to "latest" inline in the file `values.yaml`.
yq e '.spec.template.spec.tolerations += [{"key": "worker-node", "operator": "exists"}]' pod.yaml  # appends (`+=`) an array item to the array value of key `...tolerations` in `pod.yaml`.
yq ea '. as $item ireduce ({}; . *+ $item)' ~/.kube/config ~/.kube/config2  # merge two YAML files.
jq '. + input' file1.json file2.json  # merges `file1.json` with `file2.json`, any conflicts will be overwritten with the later file.
tee FILE << EOF  # write multiple lines to a new file `FILE`.
line1
line2
line3
EOF

# file compression
tar -cf ARCHIVE.tar FILE1 FILE2 DIR1 # create ARCHIVE.tar which contains FILE1 FILE2 and DIR1.
tar -xf ARCHIVE.tar  # extract from file ARCHIVE.tar. can add `-v` for verbose and `-z` for gunzip.
tar -C ./EXISTING_FOLDER -zxf TARBALL.tar.gz  # extract tarball TARBALL to EXISTING_FOLDER.
gunzip ARCHIVE.gz  # unzip gzip ARCHIVE.
bunzip2 ARCHIVE  # unzip ARCHIVE.
zip STUFF.zip A B C  # zips all local files A, B and C into STUFF.zip.
zip -e SECURE.zip *.pem *.txt  # zips all local pem and txt files into password encrypted zipfile SECURE.zip.
zipinfo ZIPFILE.zip  # list contents of a zipfile ZIPFILE.zip.
unzip STUFF.zip  # extracts contents of STUFF.zip to current folder.

# network
nc -vlp PORT  # listens on port PORT for connections.
nc -vz HOST PORT  # checks if host HOST listens for connection on port PORT
apt install -y iputils-ping  # install ping with apt.
sudo tcpdump -i any -A net 10.0.0.0/24 and port 80  # dump packets (in [A]scii) from/to network 10.0.0.0/24 on port 80, traveling through any network interface.
lsof -i 4tcp:443  # list open connections using [in example] IPv4, TCP protocol, and port 443.
curl v4.ifconfig.co  # API that returns my outward facing IP; add /country for country.
curl ifconfig.me  # API that returns my outward facing IP.
lsof -i [i]  # list all open file/connections with matching internat address [i].
dig maze.omg.lol TXT  # query TXT type records for domain 'maze.omg.lol'.
sudo arp -e -n  # show current ARP table, numerically.
nmap -sP 10.0.0.0/24  # host discovery in network 10.0.0.0/24.

# bash & scripting
set -euo pipefail  # when placed at start of bash script will exit on errors, undefined variables or broken pipes.
$(( expression to calculates ))  # [numerically ]evaluates expression.
${string:position:length}  # Extracts substring from `$string` of length `$length` at `$position`.
echo -e '\e[31mTEXT'  # prints TEXT in red (1=bold, 31-37=colors, etc.).
echo -e '\e[2JTEXT'  # prints TEXT flashing.
shuf -i 1-6 -n 1  # roll a die.
exec > >(tee -i FILEPATH) 2>&1  # redirect a script's stdout & stderr to FILEPATH for logging.
COMMAND | tee FILE  # runs COMMAND and directs output to stdout and to FILE (can be file or another program).
crunch 6 6 0123456789 -t maze%%  # returns all length 5-6 combinations of pattern mazeXX (followed by digits).
unset ENV_VAR_NAME  # delete variable ENV_VAR_NAME
SIZE=10000; \ls -l | cut -d ' ' -f 5- | awk '{if($1 > S) print $5}' S=${SIZE}  # list filenames (column 5) if their size in bytes (column 1) is greater than 10000.
sed -Ei "s/(LINE X)/\1\nLINE Y/" FILENAME  # adds LINE Y under LINE X in file FILENAME.
sed -i '2i BBB' FILENAME  # inserts BBB to line 2 of file FILENAME.
envsubst < TEMPLATE_FILE  # prints TEMPLATE_FILE after substituting any `${VAR}`s in it with the values from exported environment variables.
sort -rhk 3  # sorts data in reverse human-readable order based on 3rd column.
VAR_WITHOUT_FIRST_2_CHARS=${VAR:2}  # cuts out first 2 charactars from variable VAR.

# openssl
openssl x509  -in CERTIFICATE_FILE -text -noout  # output the contents of CERTIFICATE_FILE.
openssl x509 -req -in example.com.csr -CA my-ca.crt -CAkey my-ca.key -CAcreateserial -days 365 -out example.com.crt  # sign example.com.csr with my-ca.key
openssl rand -hex 16  # generate a random 16 byte hex strins.
openssl s_client -showcerts -tls1_2 -debug HOST:PORT  # for troubleshooting/examining tls certs
certbot certonly --webroot --webroot-path /var/www/html/ --renew-by-default --email michael.52@msn.com --text --agree-tos -d maze88.dev  # renewal of certificate for ssl with let's encrypt over cdn.

# ssh
ssh-copy-id -i ~/.ssh/a.key.pub user@host  # given a username, host and public key, it prompts for a password and adds the public key to the host's authorized_keys file.
ssh-keygen -y -f ~/.ssh/my.key > ~/.ssh/my.key.pub  # generate a public key file from a private key.
ssh -L 8385:localhost:8384 user@remotehost  # bind `localhost:8385` to `remotehost:8384`, to forward and locally access the remote socket.
# if a dropped/zombie ssh session is frozen or locked, it can be killed by pressing `<enter>~.` (source: https://askubuntu.com/questions/29942/how-can-i-break-out-of-ssh-when-it-locks).

# misc
wget https://REMOTE_FILE_URL -O LOCAL_FILENAME  # get REMOTE_FILE_URL from the web and save it locally as LOCAL_FILENAME (can remove `-O` to use remote filename).
curl -fsSL https://REMOTE_FILE_URL -o LOCAL_FILENAME  # get REMOTE_FILE_URL from the web and save it locally as LOCAL_FILENAME (can use `-O`, with no argument, to use remote filename).
curl wttr.in/london?format=4  # get one line format of weather and wind for location.
youtube-dl -x --audio-format mp3 --audio-quality 0 https://www.youtube.com/watch?v=ID  # extract and download best mp3 audio quality from link.
youtube-dl -F https://www.youtube.com/watch?v=ID  # list all available formats of requested video.
youtube-dl -f X https://www.youtube.com/watch?v=ID  # download video in format X (from `-F` list).
COMMAND | nc termbin.com 9999  # returns url to COMMAND's output for sharing with others.
catt cast https://www.youtube.com/watch?v=u3T7hmLthUU  # casts video/file to chromecast device (other sub-commands include play_toggle, stop, status, and more...). get it with `pip install catt`.
diff <(COMMAND1) <(COMMAND2)  # show the difference between the output of COMMAND1 and COMMAND2.
echo 1 | sudo tee /sys/class/leds/tpacpi::kbd_backlight/brightness  # adjust keyboard backlight to intensity 1 (can be 1-3).
# `Ctrl+w` will delete one *word* in the terminal console input.
curl -s https://ll.thespacedevs.com/2.2.0/spacestation/ | jq ".results[] | select(.owners[].abbrev==\"NASA\") | .name,.description"  # use `jq` to filter a list of api results for owners with abbreviation "NASA", and return the objects' `name` and `description` attributes.
pandoc --pdf-engine xelatex -V 'mainfont:Liberation Serif' # xelatex is the only fully supported engine for bidirectional documents; additionally its default font doesn't support all unicode so an alternative font must be set.
