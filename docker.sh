sudo usermod -aG docker $USER; newgrp docker  # append $USER to group docker, and login with that group (instead of reconnecting to session).
PYTHONUNBUFFERED=1  # when set with ENV in a dockerfile for a python image, allows continuous flow of stdout & stderr to terminal.
docker run --platform linux/amd64 IMAGE  # run an amd64 docker image on M1 mac (or other architecture).
